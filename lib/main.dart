import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/core/configurations/app.configuration.dart';
import 'package:ideomovilcoretech/core/configurations/dio.confuguration.dart';

import 'app/routes/name.route.dart';
import 'app/routes/view.route.dart';
import 'core/utils/dependency_injection.util.dart';

void main() {
  AppConfiguration.iniciarAmbiente();
  DioConfiguration.iniciarDio();
  DependencyInjectionUtil.iniciarDepndencias();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'ideo-CORETECH',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        getPages: ViewRoute.vistas,
        initialRoute: NameRoute.bienvenida
        // home: const BienvenidaView()
        );
  }
}
