import 'package:get/get.dart';

import 'menu_principal.controller.dart';

class MenuPrincipalBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MenuPrincipalController>(() => MenuPrincipalController());
  }
}
