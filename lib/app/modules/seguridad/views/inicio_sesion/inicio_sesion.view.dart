import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/components/texto_form.component.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/views/inicio_sesion/widgets/cabecera.widget.dart';
import 'package:ideomovilcoretech/core/themes/app.color.dart';

import 'inicio_sesion.controller.dart';

class InicioSesionView extends StatelessWidget {
  InicioSesionView({super.key});

/*
  TextEditingController controladorUsuario;
  TextEditingController controladorClave;
  */

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GetBuilder<InicioSesionController>(
        builder: ((controller) => WillPopScope(
              onWillPop: () async {
                SystemNavigator.pop();
                return true;
                // SystemNavigator.pop();
              },
              child: Scaffold(
                body: SingleChildScrollView(
                  child: SizedBox(
                    // width: double.infinity,
                    // height: double.infinity,
                    child: Stack(children: [
                      SafeArea(top: false, child: CabeceraWidget(size: size)),
                      Column(children: [
                        const SizedBox(height: 250),
                        Container(
                            //padding: const EdgeInsets.all(20),
                            margin: const EdgeInsets.symmetric(horizontal: 30),
                            width: double.infinity,
                            //height: 100,
                            decoration: BoxDecoration(
                                color: AppColor.oColorBlanco,
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 15,
                                      offset: Offset(0, 5))
                                ]),
                            child: Column(children: [
                              const SizedBox(
                                height: 30,
                              ),
                              const Text(
                                'BIENVENIDO',
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: AppColor.oColorPrincipal,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                  padding: const EdgeInsets.all(20),
                                  child: Column(children: [
                                    TextoFormComponent(
                                      oControlador:
                                          controller.controladorUsuario,
                                      lObscureText: false,
                                      lAutofocus: true,
                                      sHintText: 'Usuario',
                                      sLabelText: 'Código de Usuario',
                                      sPrefixIcon:
                                          const Icon(Icons.person_outline),
                                      oKeyboardType: TextInputType.emailAddress,
                                      lSoloLectura: false,
                                      oAccionChanged: (value) {
                                        //oValoresForm['scodigoacceso'] = value;

                                        //oAccesoBloc.add(CambioCodigoAccesoEvent(value));
                                      },
                                      oAccionValidator: (_) {},
                                      /* !state.lCodigoAccesoValido
                                          ? 'Usuario de Acceso No Válido'
                                          : null,*/
                                      oAccionOnTab: () {},
                                    ),
                                    const SizedBox(
                                      height: 30,
                                    ),
                                    TextoFormComponent(
                                        oControlador:
                                            controller.controladorClave,
                                        lObscureText: true,
                                        lAutofocus: false,
                                        sHintText: '******',
                                        sLabelText: 'Contraseña',
                                        sPrefixIcon:
                                            const Icon(Icons.lock_outline),
                                        oKeyboardType: TextInputType.text,
                                        lSoloLectura: false,
                                        oAccionChanged: (value) {
                                          //oValoresForm['sclaveacceso'] = value;
                                          /*  oAccesoBloc
                                          .add(CambioClaveAccesoEvent(value));*/
                                        },
                                        oAccionValidator: (_) {},
                                        /*  !state.lClaveAccesoValido
                                            ? 'Clave de Acceso No Válido'
                                            : null,*/
                                        oAccionOnTab: () {}),
                                  ])),
                              const SizedBox(
                                height: 30,
                              ),
                              Obx(
                                () => MaterialButton(
                                  onPressed: /*() {
                                  controller.iniciarSesion();
                                },*/
                                      controller.lHabilitarBoton.value
                                          ? () {
                                              controller.iniciarSesion();
                                            }
                                          : null,
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(15),
                                    bottomRight: Radius.circular(15),
                                  )),
                                  disabledColor: AppColor.oColorGrisBoton,
                                  color: AppColor.oColorPrincipal,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      Padding(
                                        padding: EdgeInsets.all(20),
                                        child: Text(
                                          'Iniciar Sesion',
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: AppColor.oColorBlanco,
                                            /*  fontFamily:
                                            constantes.sFuentePrincipalNegrita*/
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ]))
                      ]),
                    ]),
                  ),
                ),
              ),
            )));
  }
}
