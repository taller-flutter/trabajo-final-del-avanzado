import 'package:flutter/material.dart';

import 'package:ideomovilcoretech/core/themes/app.color.dart';

class CabeceraWidget extends StatelessWidget {
  const CabeceraWidget({Key? key, required this.size}) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
        // padding: EdgeInsets.all(20.0),
        // margin: EdgeInsets.all(80.00),
        alignment: Alignment.center,
        height: size.height * 0.35,
        color: AppColor.oColorPrincipal,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              'ideo-CORETECH',
              style: TextStyle(
                fontSize: 25.0,
                color: AppColor.oColorGris,
                fontWeight: FontWeight.bold,
                //  fontFamily: constantes.Fuente_Principal_Negrita
              ),
            ),
            Text(
              'Solucion integral para tu negocio...!',
              style: TextStyle(
                fontSize: 10.0,
                color: AppColor.oColorGris,
                fontWeight: FontWeight.bold,
                //  fontFamily: constantes.Fuente_Principal_Negrita
              ),
            ),
          ],
        ));
  }
}
