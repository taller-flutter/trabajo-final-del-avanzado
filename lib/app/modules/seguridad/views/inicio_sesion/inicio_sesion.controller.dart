import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/components/mensaje_modal.component.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/request/usuario.request.model.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/empresa_perfil.response.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/repositories/iniciar_sesion.repository.dart';
import 'package:ideomovilcoretech/app/routes/name.route.dart';
import 'package:ideomovilcoretech/app/services/local_storage.service.dart';
import 'package:ideomovilcoretech/core/utils/key.util.dart';

class InicioSesionController extends GetxController {
  @override
  void onInit() {
    _iniciarVista();
    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }

  //Instancias
  final _iniciarSesionRepository = Get.find<IniciarSesionRepository>();
  MensajeModalComponent oModal = MensajeModalComponent();

  //Variables
  RxBool lHabilitarBoton = RxBool(true);

  //TextEditingController
  final controladorUsuario = TextEditingController();
  final controladorClave = TextEditingController();

  //Funciones
  void _iniciarVista() {}

  void iniciarSesion() async {
    try {
      final scodigo = controladorUsuario.text;
      final scredencial = controladorClave.text;
      lHabilitarBoton.value = false;
      final response = await _iniciarSesionRepository.obtenerSesion(
          UsuarioRequestModel(scodigo: scodigo, scredencial: scredencial));

      if (response.lindError) {
        Get.dialog(
            oModal.modalMensaje(
                'Error Controlado', response.odatError!.smensajeError),
            barrierDismissible: false);
        // Get.snackbar('Error Controlado', response.odatError!.smensajeError);
        lHabilitarBoton.value = true;
        return;
      }

      if (response.lindVal) {
        Get.dialog(
            oModal.modalMensaje('Validacion', response.odatVal!.smensajeVal),
            barrierDismissible: false);
        //Get.snackbar('Validacion', response.odatVal!.smensajeVal);
        lHabilitarBoton.value = true;
        return;
      }
/*
  const param = CryptoJS.AES.encrypt(JSON.stringify(dato.orespuesta.olistaEmpresaPerfil), KEY).toString();

          this.loRutas.navigate(['/modulo/seguridad/perfil-usuario/', param]);
*/
      print("1***************************************");
      print(response.orespuesta["olistaEmpresaPerfil"].toString());

      print("2***************************************");

      // List<EmpresaPerfilResponseModel> empresaPerfilResponseModelFromJson(String str) =>

      List<EmpresaPerfilResponseModel> oListaPerfilEmpresa =
          List<EmpresaPerfilResponseModel>.from(json
              .decode(json.encode(response.orespuesta["olistaEmpresaPerfil"]))
              .map((x) => EmpresaPerfilResponseModel.fromJson(x)));

      print("***************************************");
//, arguments: response.orespuesta
      Get.offNamed(NameRoute.perfilUsuario, arguments: oListaPerfilEmpresa);
    } catch (error) {
      lHabilitarBoton.value = true;
      Get.dialog(oModal.modalMensaje('Error No Controlado', error.toString()),
          barrierDismissible: false);
      //Get.snackbar('Error', error.toString());
    }
  }
}
