import 'package:get/get.dart';

import 'inicio_sesion.controller.dart';

class InicioSesionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InicioSesionController>(() => InicioSesionController());
  }
}
