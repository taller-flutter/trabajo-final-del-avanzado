import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/routes/name.route.dart';
import 'package:ideomovilcoretech/core/themes/app.color.dart';

class InformacionUsuarioWidget extends StatelessWidget {
  const InformacionUsuarioWidget({
    Key? key,
    required this.sGeneroUsuario,
    required this.sNombreUsuario,
    required this.lIndicadorBotonAtras,
    this.sNombreOficina,
    this.sNombreEmpresa,
    this.sNombrePerfil,
  }) : super(key: key);

  final String sGeneroUsuario;
  final String sNombreUsuario;
  final bool lIndicadorBotonAtras;
  final String? sNombreEmpresa;
  final String? sNombreOficina;
  final String? sNombrePerfil;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(children: <Widget>[
        Container(
            padding: const EdgeInsets.only(top: 30),
            child: Image.asset(
              (sGeneroUsuario == 'M'
                  ? 'assets/img/usuario_m.png'
                  : 'assets/img/usuario_f.png'),
              height: 120,
              width: 120,
              color: AppColor.oColorTextoBlanco,
            )),
        Container(
          padding: const EdgeInsets.only(top: 10),
          child: Text(
            sNombreUsuario.trim(),
            style: const TextStyle(
              color: AppColor.oColorTextoBlanco,
              fontWeight: FontWeight.w700,
              fontSize: 14,
              //   fontFamily: constantes.sFuentePrincipalNormal,
            ),
            //textAlign: TextAlign.center,
          ),
        ),
        (sNombreEmpresa != null
            ? Text(
                sNombreEmpresa!.trim(),
                style: const TextStyle(
                  color: AppColor.oColorTextoBlanco,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                  //  fontFamily: constantes.sFuentePrincipalNormal,
                ),
              )
            : Container()),
        (sNombreOficina != null
            ? Text(
                sNombreOficina!.trim(),
                style: const TextStyle(
                  color: AppColor.oColorTextoBlanco,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                  //fontFamily: constantes.sFuentePrincipalNormal,
                ),
              )
            : Container()),
        (sNombrePerfil != null
            ? Text(
                sNombrePerfil!.trim(),
                style: const TextStyle(
                  color: AppColor.oColorTextoBlanco,
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                  // fontFamily: constantes.sFuentePrincipalNormal,
                ),
              )
            : Container()),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            (lIndicadorBotonAtras
                ? TextButton(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const <Widget>[
                        Icon(
                          Icons.ballot_outlined,
                          color: AppColor.oColorTextoSecundario,
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        Text(
                          'Seleccionar Perfil',
                          style: TextStyle(
                            color: AppColor.oColorTextoSecundario,
                            fontWeight: FontWeight.bold,
                            // fontFamily: constantes.sFuentePrincipalNegrit,
                          ),
                        )
                      ],
                    ),
                    onPressed: () => Navigator.of(context)
                        .popAndPushNamed('/seguridad/empresa-perfil'),
                  )
                : Container()),
            TextButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const <Widget>[
                    Icon(
                      Icons.exit_to_app,
                      color: AppColor.oColorTextoSecundario,
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Text(
                      'Cerrar Sesión',
                      style: TextStyle(
                        color: AppColor.oColorTextoSecundario,
                        fontWeight: FontWeight.bold,
                        // fontFamily: constantes.sFuentePrincipalNegrita,
                      ),
                    )
                  ],
                ),
                onPressed: () => Get.offNamed(NameRoute.inicioSesion)
                /*Navigator.of(context)
                  .popAndPushNamed('/seguridad/inicio-sesion'),*/
                ),
          ],
        )
      ]),
    );
  }
}
