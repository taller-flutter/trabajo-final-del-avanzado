import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/components/mensaje_boton_atras.component.dart';
import 'package:ideomovilcoretech/app/components/mensaje_modal.component.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/views/perfil_usuario/widgets/informacion_usuario.widget.dart';
import 'package:ideomovilcoretech/core/themes/app.color.dart';

import 'perfil_usuario.controller.dart';

class PerfilusuarioView extends StatelessWidget {
  const PerfilusuarioView({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final lindicadortmp = true;
    MensajeModalComponent oModal = MensajeModalComponent();
    MensajeBotonAtrasComponent oMensajeBotonAtras =
        MensajeBotonAtrasComponent();
    return GetBuilder<PerfilUsuarioController>(
        builder: (controller) => WillPopScope(
              onWillPop: () async {
                final shouldPop = await showDialog<bool>(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Do you want to go back?'),
                      actionsAlignment: MainAxisAlignment.spaceBetween,
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context, true);
                          },
                          child: const Text('Yes'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context, false);
                          },
                          child: const Text('No'),
                        ),
                      ],
                    );
                  },
                );
                return shouldPop!;
              },
              child: Scaffold(
                body: SingleChildScrollView(
                  child: Column(children: <Widget>[
                    Container(
                        // padding: EdgeInsets.all(20.0),
                        // margin: EdgeInsets.all(80.00),
                        alignment: Alignment.center,
                        height: size.height * 0.35,
                        color: AppColor.oColorPrincipal,
                        child: const InformacionUsuarioWidget(
                            sGeneroUsuario: 'M',
                            sNombreUsuario: 'JOSE JESUS',
                            lIndicadorBotonAtras: false)),
                    Container(
                        padding: const EdgeInsets.only(top: 20),
                        alignment: Alignment.center,
                        child: const Text(
                          "PERFILES POR EMPRESA",
                          style: TextStyle(
                            fontSize: 15.0,
                            color: AppColor.oColorTextoPrincipal,
                            fontWeight: FontWeight.bold,
                            // fontFamily: constantes.sFuentePrincipalNegrita
                          ),
                        )),
                    ListView(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 0, vertical: 20),
                        shrinkWrap: true,
                        children: <Widget>[
                          ...controller.oListaPerfilEmpresa.map((itemEmpresa) {
                            return Container(
                              color: Colors.grey[200],
                              margin: const EdgeInsets.only(bottom: 10),
                              /*decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: constantes.oColorGris
                                ),*/

                              // color: constantes.oColorGris,
                              // padding: const  EdgeInsets.all(10),
                              child: ExpansionTile(
                                  //initiallyExpanded: true,
                                  leading: const Icon(Icons.apartment_rounded),
                                  title: Text(
                                      itemEmpresa.oempresaUsuario.srazSocEmp),
                                  children: [
                                    ...itemEmpresa.olistaPerfilUsuario
                                        .map((itemPerfil) {
                                      return Card(
                                        elevation: 0,
                                        margin: const EdgeInsets.only(
                                            left: 15, right: 15, bottom: 5),
                                        child: ListTile(
                                          leading:
                                              const Icon(Icons.article_rounded),
                                          trailing: const Icon(
                                              Icons.arrow_forward_ios),
                                          title: Text(itemPerfil.sdesPerfil),
                                          // subtitle: Text('lista de perfiles '),
                                          onTap: () {},
                                        ),
                                      );
                                    })
                                  ]),
                            );
                          })
                        ])
                  ]),
                ),
              ),
            ));
  }
}
