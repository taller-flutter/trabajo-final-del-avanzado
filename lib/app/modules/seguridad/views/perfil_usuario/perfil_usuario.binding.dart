import 'package:get/get.dart';

import 'perfil_usuario.controller.dart';

class PerfilUsuarioBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PerfilUsuarioController>(() => PerfilUsuarioController());
  }
}
