import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/empresa_perfil.response.dart';

class PerfilUsuarioController extends GetxController {
  late List<EmpresaPerfilResponseModel> oListaPerfilEmpresa;

  @override
  void onInit() {
    oListaPerfilEmpresa = Get.arguments as List<EmpresaPerfilResponseModel>;

    super.onInit();
  }

  @override
  void onReady() {
    // TODO: implement onReady
    super.onReady();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}
