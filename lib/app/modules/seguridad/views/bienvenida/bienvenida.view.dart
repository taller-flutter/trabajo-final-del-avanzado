import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/routes/name.route.dart';

import 'bienvenida.controller.dart';

class BienvenidaView extends StatelessWidget {
  const BienvenidaView({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BienvenidaController>(
        builder: (controller) => Scaffold(
              body: Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                    Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.lightBlue,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        iconSize: 72,
                        icon: const Icon(Icons.settings_outlined),
                        color: Colors.white,
                        onPressed: () {
                          Get.offNamed(NameRoute.inicioSesion);
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Text(
                      'BIENVENIDO',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        //   color: constantes.oColorBlanco,
                        // constantes.sFuentePrincipalNegrita
                      ),
                    ),
                  ])),
            ));
    //
  }
}
