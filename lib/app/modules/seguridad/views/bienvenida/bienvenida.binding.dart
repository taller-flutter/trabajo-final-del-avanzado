import 'package:get/get.dart';

import 'bienvenida.controller.dart';

class BienvenidaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BienvenidaController>(() => BienvenidaController());
  }
}
