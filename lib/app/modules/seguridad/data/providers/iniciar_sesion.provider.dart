import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/usuario.response.model.dart';
import 'package:ideomovilcoretech/app/services/local_storage.service.dart';
import 'package:ideomovilcoretech/core/utils/key.util.dart';

import '../models/request/usuario.request.model.dart';
import '../models/response/respuesta.response.model.dart';

class IniciarSesionProvider {
  //final _dio = Dio();
  final _dio = Get.find<Dio>();
  late RespuestaResponseModel loRespuesta;
  late UsuarioResponseModel loUsuarioResponse;

  Future<RespuestaResponseModel> obtenerSesion(
      UsuarioRequestModel request) async {
    final response = await _dio.post("/seg/generador-token/v1/sesiones",
        data: request.toMap(),
        options: Options(headers: {
          "saplicativo": "0001",
          "stipoaplicativo": "001",
        }));
    /* print("**********************");
    print(response.headers.value("authorization").toString().substring(7));
    print("**********************");
*/

    loRespuesta = RespuestaResponseModel.fromJson(response.data);

    if (!loRespuesta.lindError && !loRespuesta.lindVal) {
      loUsuarioResponse = UsuarioResponseModel.fromJson(loRespuesta.orespuesta);

      await LocalStorageService.set(
          key: KeyUtil.sUsuarioKey,
          valor: jsonEncode(loUsuarioResponse.toJson()));

      await LocalStorageService.set(
          key: KeyUtil.sTokenKey,
          valor:
              response.headers.value("authorization").toString().substring(7));
    }

    return RespuestaResponseModel.fromJson(response.data);
  }
}
