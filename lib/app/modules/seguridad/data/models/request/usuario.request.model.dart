class UsuarioRequestModel {
  UsuarioRequestModel({
    required this.scodigo,
    required this.scredencial,
  });

  String scodigo;
  String scredencial;

  Map<String, dynamic> toMap() => {
        "scodigo": scodigo,
        "scredencial": scredencial,
      };
}
