class ErrorResponseModel {
  ErrorResponseModel({
    required this.scodigoError,
    required this.stipoError,
    required this.smensajeError,
    required this.sorigenError,
  });

  String scodigoError;
  String stipoError;
  String smensajeError;
  String sorigenError;

  factory ErrorResponseModel.fromJson(Map<String, dynamic> json) =>
      ErrorResponseModel(
        scodigoError: json["scodigoError"],
        stipoError: json["stipoError"],
        smensajeError: json["smensajeError"],
        sorigenError: json["sorigenError"],
      );

  Map<String, dynamic> toJson() => {
        "scodigoError": scodigoError,
        "stipoError": stipoError,
        "smensajeError": smensajeError,
        "sorigenError": sorigenError,
      };
}
