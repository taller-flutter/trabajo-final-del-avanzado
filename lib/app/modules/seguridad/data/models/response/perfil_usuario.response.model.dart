class PerfilUsuarioResponseModel {
  PerfilUsuarioResponseModel({
    required this.nidUsuario,
    required this.nidEmpresa,
    required this.scodPerfil,
    required this.scodigoApl,
    required this.nidRelUsuPer,
    required this.sidSesReg,
    required this.sestUsuPer,
    required this.sdesPerfil,
    required this.dfecReg,
  });

  int nidUsuario;
  int nidEmpresa;
  String scodPerfil;
  String scodigoApl;
  int nidRelUsuPer;
  String sidSesReg;
  String sestUsuPer;
  String sdesPerfil;
  DateTime dfecReg;

  factory PerfilUsuarioResponseModel.fromJson(Map<String, dynamic> json) =>
      PerfilUsuarioResponseModel(
        nidUsuario: json["nidUsuario"],
        nidEmpresa: json["nidEmpresa"],
        scodPerfil: json["scodPerfil"],
        scodigoApl: json["scodigoApl"],
        nidRelUsuPer: json["nidRelUsuPer"],
        sidSesReg: json["sidSesReg"],
        sestUsuPer: json["sestUsuPer"],
        sdesPerfil: json["sdesPerfil"],
        dfecReg: DateTime.parse(json["dfecReg"]),
      );

  Map<String, dynamic> toJson() => {
        "nidUsuario": nidUsuario,
        "nidEmpresa": nidEmpresa,
        "scodPerfil": scodPerfil,
        "scodigoApl": scodigoApl,
        "nidRelUsuPer": nidRelUsuPer,
        "sidSesReg": sidSesReg,
        "sestUsuPer": sestUsuPer,
        "sdesPerfil": sdesPerfil,
        "dfecReg": dfecReg.toIso8601String(),
      };
}
