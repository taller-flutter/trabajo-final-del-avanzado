class EmpresaUsuarioResponseModel {
  EmpresaUsuarioResponseModel({
    required this.nidUsuario,
    required this.nidEmpresa,
    required this.scodOficin,
    required this.sidSesReg,
    required this.dfecReg,
    required this.nidRelEmpUsu,
    required this.stipDocEmp,
    required this.sestadoRel,
    required this.snumDocEmp,
    required this.snombreOficina,
    required this.srazSocEmp,
  });

  int nidUsuario;
  int nidEmpresa;
  String scodOficin;
  String sidSesReg;
  DateTime dfecReg;
  int nidRelEmpUsu;
  String stipDocEmp;
  String sestadoRel;
  String snumDocEmp;
  String snombreOficina;
  String srazSocEmp;

  factory EmpresaUsuarioResponseModel.fromJson(Map<String, dynamic> json) =>
      EmpresaUsuarioResponseModel(
        nidUsuario: json["nidUsuario"],
        nidEmpresa: json["nidEmpresa"],
        scodOficin: json["scodOficin"],
        sidSesReg: json["sidSesReg"],
        dfecReg: DateTime.parse(json["dfecReg"]),
        nidRelEmpUsu: json["nidRelEmpUsu"],
        stipDocEmp: json["stipDocEmp"],
        sestadoRel: json["sestadoRel"],
        snumDocEmp: json["snumDocEmp"],
        snombreOficina: json["snombreOficina"],
        srazSocEmp: json["srazSocEmp"],
      );

  Map<String, dynamic> toJson() => {
        "nidUsuario": nidUsuario,
        "nidEmpresa": nidEmpresa,
        "scodOficin": scodOficin,
        "sidSesReg": sidSesReg,
        "dfecReg": dfecReg.toIso8601String(),
        "nidRelEmpUsu": nidRelEmpUsu,
        "stipDocEmp": stipDocEmp,
        "sestadoRel": sestadoRel,
        "snumDocEmp": snumDocEmp,
        "snombreOficina": snombreOficina,
        "srazSocEmp": srazSocEmp,
      };
}
