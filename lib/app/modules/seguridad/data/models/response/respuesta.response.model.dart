import 'error.response.model.dart';
import 'validacion.response.model.dart';

class RespuestaResponseModel {
  RespuestaResponseModel({
    required this.odatVal,
    required this.lindVal,
    required this.lindError,
    required this.odatError,
    required this.orespuesta,
  });

  ValidacionResponseModel? odatVal;
  bool lindVal;
  bool lindError;
  ErrorResponseModel? odatError;
  dynamic orespuesta;

  factory RespuestaResponseModel.fromJson(Map<String, dynamic> json) =>
      RespuestaResponseModel(
        odatVal: json["odatVal"] == null
            ? null
            : ValidacionResponseModel.fromJson(json["odatVal"]),
        lindVal: json["lindVal"],
        lindError: json["lindError"],
        odatError: json["odatError"] == null
            ? null
            : ErrorResponseModel.fromJson(json["odatError"]),
        orespuesta: json["orespuesta"],
      );
}
