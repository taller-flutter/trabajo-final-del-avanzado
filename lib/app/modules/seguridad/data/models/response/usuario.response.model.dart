import 'empresa_usuario.response.model.dart';
import 'perfil_usuario.response.model.dart';

class UsuarioResponseModel {
  UsuarioResponseModel({
    required this.nidUsuario,
    required this.scodAccUsu,
    required this.stelPerUsu,
    required this.sidSesion,
    required this.snomComUsu,
    required this.sgeneroUsu,
    required this.scorEleUsu,
    required this.oempresaSel,
    required this.operfilSel,
  });

  int nidUsuario;
  String scodAccUsu;
  String stelPerUsu;
  String sidSesion;
  String snomComUsu;
  String sgeneroUsu;
  String scorEleUsu;
  EmpresaUsuarioResponseModel oempresaSel;
  PerfilUsuarioResponseModel operfilSel;

  factory UsuarioResponseModel.fromJson(Map<String, dynamic> json) =>
      UsuarioResponseModel(
        nidUsuario: json["nidUsuario"],
        scodAccUsu: json["scodAccUsu"],
        stelPerUsu: json["stelPerUsu"],
        sidSesion: json["sidSesion"],
        snomComUsu: json["snomComUsu"],
        sgeneroUsu: json["sgeneroUsu"],
        scorEleUsu: json["scorEleUsu"],
        oempresaSel: EmpresaUsuarioResponseModel.fromJson(json["oempresaSel"]),
        operfilSel: PerfilUsuarioResponseModel.fromJson(json["operfilSel"]),
      );

  Map<String, dynamic> toJson() => {
        "nidUsuario": nidUsuario,
        "scodAccUsu": scodAccUsu,
        "stelPerUsu": stelPerUsu,
        "sidSesion": sidSesion,
        "snomComUsu": snomComUsu,
        "sgeneroUsu": sgeneroUsu,
        "scorEleUsu": scorEleUsu,
        "oempresaSel": oempresaSel.toJson(),
        "operfilSel": operfilSel.toJson(),
      };
}
