import 'dart:convert';

import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/empresa_usuario.response.model.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/perfil_usuario.response.model.dart';

class EmpresaPerfilResponseModel {
  EmpresaPerfilResponseModel({
    required this.olistaPerfilUsuario,
    required this.oempresaUsuario,
  });

  List<PerfilUsuarioResponseModel> olistaPerfilUsuario;
  EmpresaUsuarioResponseModel oempresaUsuario;

  factory EmpresaPerfilResponseModel.fromJson(Map<String, dynamic> json) =>
      EmpresaPerfilResponseModel(
        olistaPerfilUsuario: List<PerfilUsuarioResponseModel>.from(
            json["olistaPerfilUsuario"]
                .map((x) => PerfilUsuarioResponseModel.fromJson(x))),
        oempresaUsuario:
            EmpresaUsuarioResponseModel.fromJson(json["oempresaUsuario"]),
      );

  Map<String, dynamic> toJson() => {
        "olistaPerfilUsuario":
            List<dynamic>.from(olistaPerfilUsuario.map((x) => x.toJson())),
        "oempresaUsuario": oempresaUsuario.toJson(),
      };
}
