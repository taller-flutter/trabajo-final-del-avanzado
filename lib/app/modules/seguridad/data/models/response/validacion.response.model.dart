class ValidacionResponseModel {
  ValidacionResponseModel({
    required this.smensajeVal,
    required this.scodigoVal,
    required this.sorigenVal,
    required this.stipoVal,
  });

  String smensajeVal;
  String scodigoVal;
  String sorigenVal;
  String stipoVal;

  factory ValidacionResponseModel.fromJson(Map<String, dynamic> json) =>
      ValidacionResponseModel(
        smensajeVal: json["smensajeVal"],
        scodigoVal: json["scodigoVal"],
        sorigenVal: json["sorigenVal"],
        stipoVal: json["stipoVal"],
      );
}
