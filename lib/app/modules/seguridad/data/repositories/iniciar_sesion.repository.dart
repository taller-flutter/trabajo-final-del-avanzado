import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/request/usuario.request.model.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/models/response/respuesta.response.model.dart';
import 'package:ideomovilcoretech/app/modules/seguridad/data/providers/iniciar_sesion.provider.dart';

class IniciarSesionRepository {
  final _apiProvider = Get.find<IniciarSesionProvider>();

  Future<RespuestaResponseModel> obtenerSesion(UsuarioRequestModel request) =>
      _apiProvider.obtenerSesion(request);
}
