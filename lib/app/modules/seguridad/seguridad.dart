//views
export 'package:ideomovilcoretech/app/modules/seguridad/views/menu_principal/menu_principal.view.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/perfil_usuario/perfil_usuario.view.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/inicio_sesion/inicio_sesion.view.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/bienvenida/bienvenida.view.dart';

// bindings
export 'package:ideomovilcoretech/app/modules/seguridad/views/perfil_usuario/perfil_usuario.binding.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/bienvenida/bienvenida.binding.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/inicio_sesion/inicio_sesion.binding.dart';
export 'package:ideomovilcoretech/app/modules/seguridad/views/menu_principal/menu_principal.binding.dart';
