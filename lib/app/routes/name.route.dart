abstract class NameRoute {
  static const bienvenida = "/bienvenida";
  static const inicioSesion = "/incio-sesion";
  static const perfilUsuario = "/perfil-usuario";
  static const menuPrincipal = "/menu-principal";
}
