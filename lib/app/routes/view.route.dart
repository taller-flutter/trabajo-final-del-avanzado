import 'package:get/get.dart';

import '../modules/seguridad/seguridad.dart';
import 'name.route.dart';

abstract class ViewRoute {
  static final vistas = [
    GetPage(
      name: NameRoute.bienvenida,
      page: () => const BienvenidaView(),
      binding: BienvenidaBinding(),
    ),
    GetPage(
      name: NameRoute.inicioSesion,
      page: () => InicioSesionView(),
      binding: InicioSesionBinding(),
    ),
    GetPage(
      name: NameRoute.perfilUsuario,
      page: () => const PerfilusuarioView(),
      binding: PerfilUsuarioBinding(),
    ),
    GetPage(
      name: NameRoute.menuPrincipal,
      page: () => const MenuPrincipalView(),
      binding: MenuPrincipalBinding(),
    )
  ];
}
