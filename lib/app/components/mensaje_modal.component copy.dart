import 'package:flutter/material.dart';
import 'package:ideomovilcoretech/app/components/boton_opciones.component.dart';

import 'package:ideomovilcoretech/core/themes/app.color.dart';

class MensajeModalComponent {
  Future<void> modalMensaje(
      BuildContext context, String sTituloval, String sMensajeval) async {
    return showDialog<void>(
      context: context,
      barrierDismissible:
          false, // para obligar a que salga presionando el boton
      builder: (BuildContext context) {
        /* double altopantalla = MediaQuery.of(context).size.height;
      double anchopantalla = MediaQuery.of(context).size.width;*/
        return AlertDialog(
          title: Column(
            children: <Widget>[
              Text(sTituloval,
                  style: const TextStyle(
                    fontSize: 20.0,
                    color: (AppColor.oColorPrincipal),
                    fontWeight: FontWeight.bold,
                    //fontFamily: constantes.sFuentePrincipalNegrita,
                  )),
              const Divider(),
            ],
          ),
          content: Text(sMensajeval.trim(),
              style: const TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                // fontFamily: constantes.sFuentePrincipalNegrita,
              )),

          /*
        SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(_mensajeval,
                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black ,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: constantes.sFuentePrincipalNegrita
                                  )
                    ),
                    
              Divider(),
            ],
          ),
        ),*/
          actions: <Widget>[
            Column(
              children: <Widget>[
                const Divider(),
                BotonOpcionesComponent(
                    cNombreBoton: "Aceptar",
                    nAlto: 25.00,
                    nLargo: 80.00,
                    nTamTexBot: 15.0,
                    oColBot: AppColor.oColorPrincipal,
                    oColTexBot: AppColor.oColorTextoBlanco,
                    lHabilitaBoton: true,
                    oAccionBoton: () => Navigator.pop(context, false)),
              ],
            )
          ],
        );
      },
    );
  }
}
