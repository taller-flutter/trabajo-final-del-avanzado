import 'package:flutter/material.dart';

class TextoFormComponent extends StatelessWidget {
  const TextoFormComponent({
    Key? key,
    this.oControlador,
    required this.lObscureText,
    required this.lAutofocus,
    required this.sHintText,
    required this.sLabelText,
    required this.sPrefixIcon,
    required this.oKeyboardType,
    required this.lSoloLectura,
    required this.oAccionChanged,
    required this.oAccionValidator,
    required this.oAccionOnTab,
  }) : super(key: key);
  final TextEditingController? oControlador;
  final bool lObscureText;
  final bool lAutofocus;
  final String sHintText;
  final String sLabelText;
  final Widget sPrefixIcon;
  final TextInputType oKeyboardType;
  final bool lSoloLectura;
  final void Function(String) oAccionChanged;
  final String? Function(String?) oAccionValidator;
  final void Function() oAccionOnTab;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: oControlador,
      obscureText: lObscureText,
      autocorrect: false,
      autofocus: lAutofocus,
      keyboardType: oKeyboardType,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onChanged: oAccionChanged,
      validator: oAccionValidator,
      readOnly: lSoloLectura,
      decoration: InputDecoration(
        hintText: sHintText, //'Usuario',
        labelText: sLabelText, //'Código de Usuario',
        prefixIcon: sPrefixIcon, //Icon(Icons.person_outline),
      ),
      onTap: oAccionOnTab,
    );
  }
}
