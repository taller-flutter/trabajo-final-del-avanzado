import 'package:flutter/material.dart';
import 'package:ideomovilcoretech/core/themes/app.color.dart';

class BotonOpcionesComponent extends StatelessWidget {
  final String cNombreBoton;
  final double nAlto;
  final double nLargo;
  final double nTamTexBot;
  final Color oColBot;
  final Color oColTexBot;
  final bool lHabilitaBoton;
  final VoidCallback oAccionBoton;

  const BotonOpcionesComponent(
      {super.key,
      required this.cNombreBoton,
      required this.nAlto,
      required this.nLargo,
      required this.nTamTexBot,
      required this.oColBot,
      required this.oColTexBot,
      required this.lHabilitaBoton,
      required this.oAccionBoton});

  @override
  Widget build(BuildContext context) {
    final ButtonStyle flatButtonStyle = TextButton.styleFrom(
      //minimumSize: const Size(200, 100),
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      backgroundColor: lHabilitaBoton ? oColBot : AppColor.oColorGris,
    );

    return Container(
        margin: const EdgeInsets.only(
          top: 20.0,
          left: 20.0,
          right: 20.0,
        ),
        width: nLargo,
        height: nAlto,
        child: TextButton(
          style: flatButtonStyle,
          onPressed: lHabilitaBoton
              ? oAccionBoton
              : () {
                  null;
                },
          child: Text(cNombreBoton,
              style: TextStyle(
                color: oColTexBot,
                fontSize: nTamTexBot,
                //  fontFamily: constantes.sFuentePrincipalNegrita),
              )),
        ));
  }
}
