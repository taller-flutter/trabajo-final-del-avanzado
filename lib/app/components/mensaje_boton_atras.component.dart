import 'package:flutter/material.dart';
import 'package:ideomovilcoretech/app/components/boton_opciones.component.dart';
import 'package:ideomovilcoretech/core/themes/app.color.dart';

class MensajeBotonAtrasComponent {
  Future<bool> botonAtras(BuildContext context) async {
    final shouldPop = await showDialog<bool>(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return AlertDialog(
          title: Column(
            children: const <Widget>[
              Text('Alerta de Aplicativo',
                  style: TextStyle(
                    fontSize: 20.0,
                    color: (AppColor.oColorPrincipal),
                    fontWeight: FontWeight.bold,
                    //fontFamily: constantes.sFuentePrincipalNegrita
                  )),
              Divider(),
            ],
          ),

          content: const Text('¿Estas seguro que deseas cerrar sesión?',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                //fontFamily: constantes.sFuentePrincipalNegrita,
              )),

          //  title: const Text('Do you want to go back?'),
          // actionsAlignment: MainAxisAlignment.center,
          actions: [
            Column(children: [
              const Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Divider(),
                  BotonOpcionesComponent(
                      cNombreBoton: "SI",
                      nAlto: 25.00,
                      nLargo: 80.00,
                      nTamTexBot: 15.0,
                      oColBot: AppColor.oColorPrincipal,
                      oColTexBot: AppColor.oColorTextoBlanco,
                      lHabilitaBoton: true,
                      oAccionBoton: () => Navigator.popAndPushNamed(
                          context, '/seguridad/inicio-sesion')),
                  BotonOpcionesComponent(
                      cNombreBoton: "NO",
                      nAlto: 25.00,
                      nLargo: 80.00,
                      nTamTexBot: 15.0,
                      oColBot: AppColor.oColorPrincipal,
                      oColTexBot: AppColor.oColorTextoBlanco,
                      lHabilitaBoton: true,
                      oAccionBoton: () => Navigator.pop(context, false)),
                ],
              ),
            ])
          ],
        );
      },
    );
    return shouldPop!;
  }
}
