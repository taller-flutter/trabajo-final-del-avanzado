import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/app/components/boton_opciones.component.dart';

import 'package:ideomovilcoretech/core/themes/app.color.dart';

class MensajeModalComponent {
  Widget modalMensaje(String sTituloval, String sMensajeval) {
    return AlertDialog(
      title: Column(
        children: <Widget>[
          Text(sTituloval,
              style: const TextStyle(
                fontSize: 20.0,
                color: (AppColor.oColorPrincipal),
                fontWeight: FontWeight.bold,
                //fontFamily: constantes.sFuentePrincipalNegrita,
              )),
          const Divider(),
        ],
      ),
      content: Text(sMensajeval.trim(),
          style: const TextStyle(
            fontSize: 16.0,
            color: Colors.black,
            fontWeight: FontWeight.bold,
            // fontFamily: constantes.sFuentePrincipalNegrita,
          )),
      actions: <Widget>[
        Column(
          children: <Widget>[
            const Divider(),
            BotonOpcionesComponent(
                cNombreBoton: "Aceptar",
                nAlto: 25.00,
                nLargo: 80.00,
                nTamTexBot: 15.0,
                oColBot: AppColor.oColorPrincipal,
                oColTexBot: AppColor.oColorTextoBlanco,
                lHabilitaBoton: true,
                oAccionBoton: () {
                  Get.back();
                  // Navigator.pop(context, false);
                }),
          ],
        )
      ],
    );
  }
}
