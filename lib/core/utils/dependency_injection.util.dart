import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

import '../../app/modules/seguridad/data/providers/iniciar_sesion.provider.dart';
import '../../app/modules/seguridad/data/repositories/iniciar_sesion.repository.dart';

abstract class DependencyInjectionUtil {
  static void iniciarDepndencias() {
// LOCAL STORAGE
    Get.lazyPut<FlutterSecureStorage>(() => const FlutterSecureStorage());

// PROVIDERS
    //Get.lazyPut<IniciarSesionProvider>(() => IniciarSesionProvider());
    Get.put(IniciarSesionProvider());
//

// REPOSITORIES
    // Get.lazyPut<IniciarSesionRepository>(() => IniciarSesionRepository());
    Get.put(IniciarSesionRepository());
  }
}
