enum ApiUrl { sDESARROLLO, sCALIDAD, sPRODUCCION }

extension Environment on ApiUrl {
  static ApiUrl type = ApiUrl.sDESARROLLO;
  static Map<String, dynamic> get appConfig {
    switch (type) {
      case ApiUrl.sDESARROLLO:
        return {
          "Environment": "DESARROLLO",
          "baseUrl": "http://localhost:2820/api/ideo",
          "connectTimeout": 60000,
          "receiveTimeout": 60000,
          "sendTimeout": 60000,
        };
      case ApiUrl.sCALIDAD:
        return {
          "Environment": "CALIDAD",
          "baseUrl": "http://localhost:2820/api/ideo",
          "connectTimeout": 60000,
          "receiveTimeout": 60000,
          "sendTimeout": 60000,
        };
      case ApiUrl.sPRODUCCION:
        return {
          "Environment": "PRODUCCION",
          "baseUrl": "http://localhost:2820/api/ideo",
          "connectTimeout": 60000,
          "receiveTimeout": 60000,
          "sendTimeout": 60000,
        };
      default:
        return {};
    }
  }
}
