import 'package:flutter/material.dart';

abstract class AppColor {
  static const Color oColorPrincipal = Color(0xFF02b5e7);
  static const Color oColorBlanco = Color(0xFFFFFFFF);
  static const Color oColorGris = Color(0xFFe0e0e0);
  static const Color oColorGrisBoton = Color(0xFF878787);
  static const Color oColorAzulBoton = Color(0xFF0d47a1);
  static const Color oColorRojoBoton = Color(0xFFff1744);

  static const Color oColorTextoPrincipal = Color(0xFF212121);
  static const Color oColorTextoSecundario = Color(0xFF757575);
  static const Color oColorTextoBlanco = Color(0xFFFFFFFF);
}
