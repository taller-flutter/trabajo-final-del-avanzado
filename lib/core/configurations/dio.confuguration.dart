import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:ideomovilcoretech/core/environments/api_url.environment.dart';
import 'package:ideomovilcoretech/core/interceptors/logger.interceptor.dart';

class DioConfiguration {
  static void iniciarDio() {
    Get.lazyPut<Dio>(
      () => Dio(
        BaseOptions(
          baseUrl: Environment.appConfig["baseUrl"],
          connectTimeout: Environment.appConfig["connectTimeout"],
          receiveTimeout: Environment.appConfig["receiveTimeout"],
          sendTimeout: Environment.appConfig["sendTimeout"],
          responseType: ResponseType.json,
        ),
      )..interceptors.addAll([
          LoggerInterceptor(),
        ]),
    );
  }
}
